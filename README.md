# Dotfiles

## Clone the project

`cd ~ && git clone git@gitlab.com:jpbarbaud/dotfiles.git .dotfiles`
`cd ~/.dotfiles`

## MacOs

### Prequisite

#### Install Brew

`/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`

#### Install python with brew

`brew install python`

#### Install pyyaml with pip3

`pip3 install --user pyyaml`
`pip3 install --user pynvim`
`pip3 install --user neovim``

### Install Mac profile

`./install-profile mac`
