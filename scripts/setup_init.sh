#!/bin/sh

set -e

dotdir=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)/..

# Files ending with .ini are copied if they do not already exist
inifiles=`find $dotdir -maxdepth 3 -type f -and -name '*.ini'`
for i in $inifiles; do
  new_name=${i%.ini}
  if [ ! -f $new_name ]; then
    cp $i $new_name
  fi
done
